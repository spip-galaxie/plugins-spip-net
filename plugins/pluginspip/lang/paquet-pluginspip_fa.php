<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-pluginspip?lang_cible=fa
// ** ne pas modifier le fichier **

return [

	// P
	'pluginspip_description' => 'اين پلاگين نسخه‌ي «2011» اسكلت سايت «پلاگين‌هاي اسپيپ» متعلق به كهكشان اسپيپ است. 
-اين پلاگين، ضمن استفاه عمده از پلاگين SVP، اجازه مي‌دهد تمام اطلاعات پلاگين‌هاي اسپيپ به صفحه‌هايي كه به صورت خودكار به رورزرساني و تصويب شده‌، بازگردند.  
',
	'pluginspip_slogan' => 'اسكلت از سايت پلگين‌هاي اسپيپ توسط SVP به كار افتاده است', # MODIF
];
