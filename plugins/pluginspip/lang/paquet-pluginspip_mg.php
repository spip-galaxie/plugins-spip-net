<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-pluginspip?lang_cible=mg
// ** ne pas modifier le fichier **

return [

	// P
	'pluginspip_description' => 'Ce plugin est la version 2011 du squelette du site Plugins SPIP appartenant à la galaxie SPIP. 
_ Il permet, en utilisant principalement le plugin SVP, de restituer toutes les informations des plugins SPIP dans des pages adaptées et mises à jour automatiquement.',
	'pluginspip_slogan' => 'Squelette Z du site Plugins SPIP motorisé par SVP',
];
