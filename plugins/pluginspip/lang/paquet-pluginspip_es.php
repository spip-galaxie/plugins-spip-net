<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-pluginspip?lang_cible=es
// ** ne pas modifier le fichier **

return [

	// P
	'pluginspip_description' => 'Este plugin es la versión 2011 del esqueleto del sitio Plugins SPIP perteneciente a la galaxia SPIP. 
_ Permite, utilizando principalmente el plugin SVP, restituir toda la información de los plugins SPIP en las páginas adaptadas y actualizadas automáticamente.',
	'pluginspip_slogan' => 'Esqueleto Z del sitio Plugins SPIP motorizado por SVP',
];
