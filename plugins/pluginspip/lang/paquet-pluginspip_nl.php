<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-pluginspip?lang_cible=nl
// ** ne pas modifier le fichier **

return [

	// P
	'pluginspip_description' => 'Deze plugin is versie 2011 van het skelet van de site Plugins SPIP, onderdeel van de SPIP galaxy. 
_ Het verschaft, hoofdzakelijk gebruikmakend van plugin SVP, om alle informatie over SPIP plugins weer te geven op specifieke pagina’s die automatisch worden aangepast.',
	'pluginspip_slogan' => 'Skelet Z van site Plugins SPIP aangedreven door SVP',
];
